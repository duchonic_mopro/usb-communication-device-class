/**
  \file               dataCommSpecs.h

  \ingroup            communication

  \brief              Datamodul between Managers

  \b Customer:        SwissInvent

  \b Project:         Econdry Dispenser Board

  \b Targetsystem:    STM32F105

  \b Compiler:        GCC 7 2017-q4-major

  \b Created:         10.07.2018, N.Duchoud

  \b Copyright:       (C) Micropool GmbH

  \copydoc            communication


 */

#ifndef INC_MANAGER_DATACOMMSPECS_H_
#define INC_MANAGER_DATACOMMSPECS_H_

#include "main.h"

#define ID_TX_SYSTEM_HEARTBEAT       0x00  /**< @brief Econdry Heartbeat (from Dispenser to Mainboard) */
#define ID_TX_SYSTEM_INFO            0x01
#define ID_TX_SYSTEM_DATA            0x02
#define ID_TX_SYSTEM_ERROR           0x03
#define ID_RX_SYSTEM_CMD             0x04  /**< @brief EconDry Cmd (From Mainboard to Dispenser) */

#define ID_TX_SHRED_HEARTBEAT         0x10  /**< @brief Shredder Heartbeat  */
#define ID_TX_SHRED_INFO              0x11  /**< @brief Shredder Info       */
#define ID_TX_SHRED_CURRENT_DATA      0x12
#define ID_TX_SHRED_CONTROLLER_DATA   0x13
#define ID_TX_SHRED_SENSOR_DATA       0x14
#define ID_TX_SHRED_ERROR             0x15
#define ID_RX_SHRED_MOT9960A_CMD      0x16
#define ID_RX_SHRED_MOT9960B_CMD      0x17
#define ID_RX_SHRED_MOT8412_CMD       0x18

#define ID_TX_DISPENS_HEARTBEAT       0x20  /**< @brief Shredder Heartbeat  */
#define ID_TX_DISPENS_INFO            0x21  /**< @brief Shredder Info       */
#define ID_TX_DISPENS_CURRENT_DATA    0x22
#define ID_TX_DISPENS_CONTROLLER_DATA 0x23
#define ID_TX_DISPENS_SENSOR_DATA     0x24
#define ID_TX_DISPENS_ERROR           0x25
#define ID_RX_DISPENS_MOT9960A_CMD    0x26
#define ID_RX_DISPENS_MOT9960B_CMD    0x27
#define ID_RX_DISPENS_MOT8412_CMD     0x28

#ifdef main_DEBUGCMD_ACTIVE
  #define ID_RX_SHRED_DEBUG           0x30
  #define ID_RX_SHRED_DEBUG_ERROR     0x31
  #define ID_RX_SHRED_DEBUG_8412      0x32
  #define ID_RX_SHRED_DEBUG_9960      0x33
  #define ID_RX_SHRED_DEBUG_9960A     0x34
  #define ID_RX_SHRED_DEBUG_9960B     0x35
#endif

#ifdef main_DEBUGCMD_ACTIVE
  #define ID_RX_DISPENS_DEBUG         0x40
  #define ID_RX_DISPENS_DEBUG_ERROR   0x41
  #define ID_RX_DISPENS_DEBUG_8412    0x42
  #define ID_RX_DISPENS_DEBUG_9960    0x43
  #define ID_RX_DISPENS_DEBUG_9960A   0x44
  #define ID_RX_DISPENS_DEBUG_9960B   0x45
#endif

#define data_HEARTBEAT_TIMEOUT_50ms   40


struct __attribute__((__packed__)) data_HeartBeat_struct
{
  uint16_t  counter;
};

struct __attribute__((__packed__)) data_Info_struct
{
  uint8_t   major;
  uint8_t   minor;
  uint8_t   build;
  uint8_t   revision;
};

struct __attribute__((__packed__)) data_Mot9960_struct
{
  uint8_t   start;
  uint8_t   stop;
  uint8_t   direction;
  uint8_t   timeout;
  uint8_t   error;
};

struct __attribute__((__packed__)) data_Mot8412_struct
{
  uint8_t   start;
  uint8_t   stop;
  uint8_t   direction;
  uint8_t   timeout;
  uint8_t   error;
};

struct __attribute__((__packed__)) data_SensorData_struct
{
  uint8_t   motor8412OverTemp;
  uint8_t   motor8412FaultState;
  uint8_t   tissueDetction;
  uint8_t   userDetection;
  uint8_t   userButton1;
  uint8_t   userButton2;
};

struct data_BoardDataInfo_struct
{
  bool_t motor8412running : 1;
  bool_t motor9960Arunning : 1;
  bool_t motor9960Brunning : 1;
  bool_t reserve : 13;
};
struct data_BoardDataWarnings_struct
{
  bool_t userDetectionJammed : 1;
  bool_t userButton1Jammed : 1;
  bool_t userButton2Jammed : 1;
  bool_t reserve : 13;
};
struct data_BoardDataUncriticalError_struct
{
  bool_t motor8412Overcurrent : 1;
  bool_t motor9960AOvercurrent : 1;
  bool_t motor9960BOvercurrent : 1;
  bool_t reserve : 13;
};
struct data_BoardDataCriticalError_struct
{
  bool_t motor8412Error : 1;
  bool_t motor9960AError : 1;
  bool_t motor9960BError : 1;
  bool_t reserve : 13;
};

struct __attribute__((__packed__)) data_EconDryData_struct
{
  struct data_BoardDataInfo_struct info;
  struct data_BoardDataWarnings_struct warning;
  struct data_BoardDataUncriticalError_struct UncriticalError;
  struct data_BoardDataCriticalError_struct CriticalError;
};

struct __attribute__((__packed__)) data_CurrentData_struct
{
  uint16_t   motor8412Current_mA;
  uint16_t   motor9960ACurrent_mA;
  uint16_t   motor9960BCurrent_mA;
};

struct __attribute__((__packed__)) data_ControllerData_struct
{
  uint16_t   vref;
  uint16_t   tempIntC;
};


#ifdef main_DEBUGCMD_ACTIVE
struct __attribute__((__packed__)) data_DebugCmd_struct
{
  uint8_t    motorCycleTestOn;
  uint8_t    motorCycleTestOff;
  uint8_t    tissueDetection;
  uint8_t    userDetected;
  uint8_t    userButton1Pressed;
  uint8_t    userButton2Pressed;
};

struct __attribute__((__packed__)) data_DebugSetError_struct
{
  uint8_t    info;
  uint8_t    warning;
  uint8_t    non_critical;
  uint8_t    critical;
};

struct __attribute__((__packed__)) data_Debug8412_struct
{
  uint8_t    pwmA;
  uint8_t    pwmB;
  uint8_t    nResetAB;
  uint8_t    nResetCD;
};

struct __attribute__((__packed__)) data_Debug9960_struct
{
  uint8_t    disable;
};

struct __attribute__((__packed__)) data_Debug9960x_struct
{
  uint8_t    pwm;
  uint8_t    nDis;
  uint8_t    direction;
  uint8_t    spiChipSelect;
};
#endif

#endif /* INC_MANAGER_DATACOMMSPECS_H_ */
