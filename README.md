# dataCommSpecs.h
See header file dataCommSpecs.h for full documentation of commands
```
<'t'> + <id> + <length> + <data> + <\r>
```
## example heartbeat 't01020401'
```
<'t'> transmit can msg
<id> could be ID_TX_SHRED_HEARTBEAT 0x010
<length> data length 2 (from 0 to 8 bytes)
<data> data 0x04 and 0x01 is 0x0104 value (from 0 to 8 bytes)
<\r> CR (0x0D)
```
# Beaglebone und Raspberry Pi Test

Connected the STM32F105 USB (Communication Device Class (Virtual Port Com) to a beaglebone and raspberry

## Angstrom

```
#!terminal
.---O---.
|       |                  .-.           o o
|   |   |-----.-----.-----.| |   .----..-----.-----.
|       |     | __  |  ---'| '--.|  .-'|     |     |
|   |   |  |  |     |---  ||  --'|  |  |  '  | | | |
'---'---'--'--'--.  |-----''----''--'  '-----'-'-'-'
                -'  |
                '---'
The Angstrom Distribution beaglebone ttyO0
Angstrom v2012.12 - Kernel 3.8.13
```
## Linux
```
Linux raspberrypi 4.14.34-v7+ #1110 SMP Mon Apr 16 15.18.51 BST 2018 armv7l GNU/Linux
```

## Connecting USB
There are some error codes while connecting the usb device
```
#!terminal
[   35.821756] usb 1-1: string descriptor 0 read error: -71
[   35.833734] usb 1-1: can't set config #2, error -32
[   42.622773] usb 1-1: device descriptor read/64, error -71
[   42.837663] usb 1-1: device descriptor read/64, error -71
[   43.157946] usb 1-1: device descriptor read/64, error -71
[   43.372794] usb 1-1: device descriptor read/64, error -71
[   43.605365] cdc_acm 1-1:1.0: This device cannot do calls on its own. It is not a modem.
[   52.055996] cdc_acm 1-1:1.0: This device cannot do calls on its own. It is not a modem.
```
## Starting Screen from USB
The cdc_acm driver will manage the communication. A screen with ttyACM0 will be fine to receive serial data from the STM32F105 usb.
```
#!terminal

root@beaglebone:/dev# ls tty*
tty    tty14  tty20  tty27  tty33  tty4   tty46  tty52  tty59  tty8     ttyS2
tty0   tty15  tty21  tty28  tty34  tty40  tty47  tty53  tty6   tty9     ttyS3
tty1   tty16  tty22  tty29  tty35  tty41  tty48  tty54  tty60  ttyACM0
tty10  tty17  tty23  tty3   tty36  tty42  tty49  tty55  tty61  ttyGS0
tty11  tty18  tty24  tty30  tty37  tty43  tty5   tty56  tty62  ttyO0
tty12  tty19  tty25  tty31  tty38  tty44  tty50  tty57  tty63  ttyS0
tty13  tty2   tty26  tty32  tty39  tty45  tty51  tty58  tty7   ttyS1
root@beaglebone:/dev# lsusb
Bus 001 Device 004: ID 0483:5740 SGS Thomson Microelectronics
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
root@beaglebone:/dev# screen ttyACM0 115200
```
The raspberry shows a different thing with lsusb
```
...
BUS 001 Device 012: ID 0483:5740 STMicroeletronics STM32F407
...
```

### Receiving data
The serial data (rx) from the STM32F105 looks like this. The boards sends a heartbeat every now and then...
```
#!terminal
t01022A05
t0202D306
t0202D406
t01022B05
t0202D906
t01022D05
t0202DA06
```
The data (rx) means

```
#!terminal
t01022D05
-> t   'transmit can msg'
-> 010 'heartbeat from shredder (counter data)'
-> 2   'data length'
-> 2D  '0x..2D of counter'
-> 05  '0x05.. of counter'
```
### Sending data
Sending data with 't0110\r' gets the sw version of the shredder
```
#!terminal
't0110\r'     (tx) 
t011400030400 (rx)
-> t   'trasmint can msg'
-> 011 'shredder sw version'
-> 4   'data length'
-> 00  '00.xx.xx.xx (major)'
-> 03  'xx.03.xx.xx (minor'
-> 04  'xx.xx.04.xx (build)'
-> 00  'xx.xx.xx.00 (revision)'
```